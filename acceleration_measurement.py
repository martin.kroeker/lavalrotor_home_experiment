import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_waschmaschine.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

acceleration_x = []
acceleration_y = []
acceleration_z = []
timestamp = []
RawData = {sensor_settings_dict["ID"] : {
    "acceleration_x" : acceleration_x,
    "acceleration_y" : acceleration_y,
    "acceleration_z" : acceleration_z,
    "timestamp": timestamp}
             }
             
print("RawData")
print(RawData)
#DONE
# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
i2c = board.I2C()  # uses board.SCL and board.SDA
accelerometer = adafruit_adxl34x.ADXL345(i2c)
start_time = time.time()

while time.time() - start_time < measure_duration_in_s:
    measurement = accelerometer.acceleration
    measurement_time = time.time()
    RawData[sensor_settings_dict["ID"]]["acceleration_x"].append(measurement[0])
    RawData[sensor_settings_dict["ID"]]["acceleration_y"].append(measurement[1])
    RawData[sensor_settings_dict["ID"]]["acceleration_z"].append(measurement[2])
    RawData[sensor_settings_dict["ID"]]["timestamp"].append(measurement_time)
    # Pause for 1ms
    time.sleep(0.001)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
print("RawData")
print(RawData)
# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# open example.h5 for write data in to.
with h5py.File(path_h5_file, 'w') as hdf_file:
    # Create a group for the sensor ID
    sensor_group = hdf_file.create_group(sensor_settings_dict["ID"])

    # Write the acceleration data
    sensor_group.create_dataset("acceleration_x", data=RawData[sensor_settings_dict["ID"]]["acceleration_x"])
    sensor_group.create_dataset("acceleration_y", data=RawData[sensor_settings_dict["ID"]]["acceleration_y"])
    sensor_group.create_dataset("acceleration_z", data=RawData[sensor_settings_dict["ID"]]["acceleration_z"])

    # Convert the timestamp list to a numpy array before writing
    timestamp_array = np.array(RawData[sensor_settings_dict["ID"]]["timestamp"])
    sensor_group.create_dataset("timestamp", data=timestamp_array)

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
