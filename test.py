import h5py
import numpy as np
import json
import os

#matplotlib inline
import matplotlib.pyplot as plt

from functions.m_operate import evaluate_metadata

from functions.m_postprocessing import get_vec_accel
from functions.m_postprocessing import interpolation
from functions.m_postprocessing import my_fft

path = os.path.join("supplementary_code", "FST.mplstyle")
plt.style.use(path)



#Experiment 1 ---------------------------------------------------------------------------------------------------------------------------
"""Parameter definition"""
path_measurement_folder = "measurement_data/data_20240104_162004_MartinKroeker_phone"
h5_file_name = "data_20240104_162004_MartinKroeker_phone.h5"



"""Prepare Metadata"""
(setup_json_dict, sensor_settings_dict, probe_name) = evaluate_metadata(
    path_measurement_folder
)

print(json.dumps(setup_json_dict, indent=2, default=str))
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print("Probe: {}".format(probe_name))



# import measurement data from h5
with h5py.File(path_measurement_folder+"/"+h5_file_name, 'r') as hdf_file:
    # Access the group for the sensor ID
    sensor_group = hdf_file[sensor_settings_dict["ID"]]

    # Read acceleration data into NumPy arrays
    acceleration_x = np.array(sensor_group["acceleration_x"])
    acceleration_y = np.array(sensor_group["acceleration_y"])
    acceleration_z = np.array(sensor_group["acceleration_z"])
    
    # Read timestmap data into NumPy arrays
    timestamp = np.array(sensor_group["timestamp"])


#absolute acceletation
data_raw = get_vec_accel(acceleration_x, acceleration_y, acceleration_z)
mean = np.mean(data_raw)
data = data_raw - mean

#plot absolute acceleration
plt.plot(timestamp, data_raw)
plt.title("Experiment 1: Vibration in Time Domain")
plt.xlabel("Timestamp (s)")
plt.ylabel("Acceleration (m/s²)")



#interpolate data
interpol_data = interpolation(timestamp, data)

#FFT data  
frequency_domain_data = my_fft(interpol_data[1] ,interpol_data[0])
print(frequency_domain_data)

#polt in frequency domain
plt.figure()
plt.plot(frequency_domain_data[1], frequency_domain_data[0])
plt.title("Experiment 1: Vibration in Frequency Domain")
plt.xlabel("Frequency (Hz)")
plt.ylabel("Amplitude")
plt.show()


#Experiment 2 -----------------------------------------------------------------------------------------------------------------------------------
"""Parameter definition"""
path_measurement_folder = "measurement_data/data_20240102_171259_MartinKroeker_waschmaschine"
h5_file_name = "data_20240102_171259_MartinKroeker_waschmaschine.h5"



"""Prepare Metadata"""
(setup_json_dict, sensor_settings_dict, probe_name) = evaluate_metadata(
    path_measurement_folder
)

print(json.dumps(setup_json_dict, indent=2, default=str))
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print("Probe: {}".format(probe_name))



# import measurement data from h5
with h5py.File(path_measurement_folder+"/"+h5_file_name, 'r') as hdf_file:
    # Access the group for the sensor ID
    sensor_group = hdf_file[sensor_settings_dict["ID"]]

    # Read acceleration data into NumPy arrays
    acceleration_x = np.array(sensor_group["acceleration_x"])
    acceleration_y = np.array(sensor_group["acceleration_y"])
    acceleration_z = np.array(sensor_group["acceleration_z"])
    
    # Read timestmap data into NumPy arrays
    '''In my first meassuring attempt I was missing () on the time.time() function,
    which is why I havent got a timestamp in the h5 file, so I will se If using the one from the previus experimnet works.'''
    #timestamp = np.array(sensor_group["timestamp"])


#absolute acceletation
data_raw = get_vec_accel(acceleration_x, acceleration_y, acceleration_z)
mean = np.mean(data_raw)
data = data_raw - mean

#adjust the timestamp to the actuall measurement
target_entries = len(data)
timestamp_c = np.linspace(0, 20, target_entries)
#Here a a fault in the result is created, because it is assumed that the individual datapoints are allready interpolated

print(timestamp_c[0])
           
#plot absolute acceleration
plt.plot(timestamp_c, data_raw)
plt.title("Experiment 2: Vibration in Time Domain")
plt.xlabel("Timestamp (s)")
plt.ylabel("Accelretaion (m/s²)")



#interpolate data
interpol_data = interpolation(timestamp_c, data)

#FFT data  
frequency_domain_data = my_fft(interpol_data[1] ,interpol_data[0])

#polt in frequency domain
plt.figure()
plt.plot(frequency_domain_data[1], frequency_domain_data[0])
plt.title("Experiment 2: Vibration in Frequency Domain")
plt.xlabel("Frequency (Hz)")
plt.ylabel("Amplitude")
plt.show()
